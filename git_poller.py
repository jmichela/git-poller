"""
  Poll a git repo and then run a follow-up command
"""
from datetime import datetime
import getopt
from git import RemoteProgress
from git import Repo
import os
import subprocess
import sys
import yaml

subdirectory = ''

def log( log_text, print_time = True ):
    f = open( subdirectory + '/log.txt', 'a')

    if print_time:
        f.write(str(datetime.now()) + ' - ')
    f.write(log_text + '\n')

    f.close()

def run_command( config ):
    if config[ 'follow-up-command' ] is None:
        log( 'No follow up command to run' )
    else:
        log( 'Running follow up command: ' + config[ 'follow-up-command' ] )

        proc = subprocess.Popen(
            config[ 'follow-up-command' ], \
            stdout=subprocess.PIPE, \
            stderr=subprocess.PIPE, \
            shell=True, \
            universal_newlines=True)
        (out, err) = proc.communicate()

        if out != '':
            log( 'Output from follow up command:\n\t' + out[:-1].replace('\n', '\n\t') )
        if err != '':
            log( 'Errors from follow up command:\n\t' + err[:-1].replace('\n', '\n\t') )

def init( config, repo_dir ):
    os.makedirs( repo_dir )
    log( 'Repo directory made' )

    repo_url = 'https://%s:%s@%s' % (
        config[ 'git' ][ 'username' ], config[ 'git' ][ 'password' ], config[ 'git' ][ 'remote-repo' ] )
    log( 'Start pulling down repo' )
    repo = Repo.clone_from(repo_url, repo_dir, branch=config[ 'git' ][ 'branch' ])
    log( 'Done pulling down repo. Current revision: ' + repo.rev_parse( config[ 'git' ][ 'branch' ] ).hexsha )

def try_init( config, repo_dir ):
    if not os.path.exists( repo_dir ):
        init( config, repo_dir )
        run_command( config )
        return True
    else:
        return False

def main(argv):
    global subdirectory

    try:
        opts, args = getopt.getopt(argv, 'h:s:', )
    except getopt.GetoptError:
        print( 'usage: git_poller.py -s <subdirectory>' )
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print( 'usage: git_poller.py -s <subdirectory>' )
            sys.exit()
        elif opt == '-s':
            subdirectory = arg

    config = yaml.load( open( subdirectory + '/config.yaml', 'r' ) )
    repo_dir = subdirectory + '/local_repo'

    if not try_init( config, repo_dir ):
        repo = Repo( repo_dir )
        repo.remotes.origin.fetch()

        current_revision = repo.rev_parse( config[ 'git' ][ 'branch' ] )
        latest_revision = repo.rev_parse( 'origin/%s' % config[ 'git' ][ 'branch' ] )

        if not current_revision.hexsha == latest_revision.hexsha:
            log( 'Updating ' + current_revision.hexsha + ' to latest revision ' + latest_revision.hexsha )

            repo.remotes.origin.pull()
            repo.head.reset(index=True, working_tree=True)

            run_command( config )
        else:
            log( 'Already at most current revision: ' + current_revision.hexsha )

    log( '--------------------------', print_time=False )

if __name__ == '__main__':
    main( sys.argv[1:] )
